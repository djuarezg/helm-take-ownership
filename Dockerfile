FROM scratch 
# Add a previously built app binary to an empty image
# To build the binary:
COPY main  /
CMD ["/main"]
